package de.fhbingen.epro.vl3.di;

import de.fhbingen.epro.vl3.model.Candidate;

public class VotingBooth {

    VoteRecorder recorder = null;

    public void setVoteRecorder(VoteRecorder recorder) {
        this.recorder = recorder;
    }

    public void vote(Candidate candidate) {
        recorder.record(candidate);
    }

}
