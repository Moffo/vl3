package de.fhbingen.epro.vl3.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.fhbingen.epro.vl3.ioc.basic.DefaultMessage;

@Configuration
public class BaseConfiguration {

	@Bean
	public DefaultMessage defaultMessage() {
		return new DefaultMessage();
	}
	
}
