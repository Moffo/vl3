package de.fhbingen.epro.vl3.configuration;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.util.Assert;

import de.fhbingen.epro.vl3.ioc.basic.DefaultMessage;

public class ProjectStartXmlConfig {

	public static void main(String[] args) {
		ApplicationContext ctx = 
			      new ClassPathXmlApplicationContext("/application-context.xml");
		
		Assert.notNull(ctx);
		
		DefaultMessage defaultMessage = (DefaultMessage) ctx.getBean("defaultMessage");
		
		Assert.notNull(defaultMessage);
	}

}
