package de.fhbingen.epro.vl3.ioc.autowiring;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

public class DefaultMessage {
	
	@Autowired
	private AlternateMessage alternateMessage;

	private List<String> messages = null;

	/**
	 * Constructor
	 */
	public DefaultMessage(List<String> messages) {
		this.messages = messages;
	}

	/**
	 * Gets message.
	 */
	public List<String> getMessage() {
		return messages;
	}

	/**
	 * Sets message.
	 */
	public void setMessage(List<String> messages) {
		if (messages.size() == 0)
			this.messages = Arrays.asList(alternateMessage.getAlternativeMessage());
		else 
			this.messages = messages;
	}

}
