package de.fhbingen.epro.vl3.ioc.list;

import java.util.List;

public class DefaultMessage {

	private List<String> messages = null;

	/**
	 * Constructor
	 */
	public DefaultMessage(List<String> messages) {
		this.messages = messages;
	}

	/**
	 * Gets message.
	 */
	public List<String> getMessage() {
		return messages;
	}

	/**
	 * Sets message.
	 */
	public void setMessage(List<String> messages) {
		this.messages = messages;
	}

}
