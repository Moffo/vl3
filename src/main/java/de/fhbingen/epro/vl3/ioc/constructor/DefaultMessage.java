package de.fhbingen.epro.vl3.ioc.constructor;

public class DefaultMessage {

	private String message = null;

	/**
	 * Constructor
	 */
	public DefaultMessage(String message) {
		this.message = message;
	}

	/**
	 * Gets message.
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets message.
	 */
	public void setMessage(String message) {
		this.message = message;
	}

}
